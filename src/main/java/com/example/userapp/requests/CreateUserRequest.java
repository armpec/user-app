package com.example.userapp.requests;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateUserRequest {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Range(min = 1, max = 150)
    private Integer age;
}
