package com.example.userapp.entities;

import com.example.userapp.requests.CreateUserRequest;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class User {

    @Id
    private String _id;

    private String firstName;

    private String lastName;

    private Integer age;

    public User() {
    }

    public User(CreateUserRequest request) {
        this.firstName = request.getFirstName();
        this.lastName = request.getLastName();
        this.age = request.getAge();
    }
}
