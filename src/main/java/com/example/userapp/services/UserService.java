package com.example.userapp.services;

import com.example.userapp.entities.User;
import com.example.userapp.repositories.UserRepository;
import com.example.userapp.requests.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User createUser(CreateUserRequest request) {
        return userRepository.save(new User(request));
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }
}
